package com.bcr.mvctest.form;
/**
 * 登录表单类
 * @todo TODO
 * @author bcr
 * @date 2016年6月24日
 */
public class LoginForm extends ActionForm {
	private String username = "";
	private String password = "";
	//默认构造方法
	public LoginForm(){}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	};
	
}
