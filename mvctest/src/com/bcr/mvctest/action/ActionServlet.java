package com.bcr.mvctest.action;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bcr.mvctest.form.ActionForm;
import com.bcr.mvctest.utils.FullBean;

public class ActionServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		
		//此处可以利用xml配置文件，配置Form与Action之间的关系
		
		
		String formClassName = "com.bcr.mvctest.form.LoginForm";//此处指定Form的类名
		//装载FormBean
		ActionForm form = FullBean.fullFormByReq(formClassName,req);
		//定义处理接口
		String actionClassName = "com.bcr.mvctest.action.LoginAction";//此处指定action的类名
		//用具体实现类实例化Action接口
		Action action = null;
		try {
			action = (Action) Class.forName(actionClassName).newInstance();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//调用接口的方法
		//String resStr =  action.execute(form);
		//System.out.println(resStr);
		
//		PrintWriter out = resp.getWriter();
//		out.print(resStr);
//		out.flush();
//		out.close();
		
		boolean checkResult = action.execute2(form);
		
		//获得转发器对象
		RequestDispatcher dis = null ;
		if(checkResult){
			dis =  req.getRequestDispatcher("/view/Success.jsp");
		}
		else{
			dis =  req.getRequestDispatcher("/view/Error.jsp");
		}
		//转发
		dis.forward(req, resp);
		
		
		
		
		

	}
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(req, resp);
	}

}
