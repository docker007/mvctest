package com.bcr.mvctest.action;

import javax.servlet.RequestDispatcher;

import com.bcr.mvctest.form.ActionForm;
import com.bcr.mvctest.form.LoginForm;
import com.bcr.mvctest.service.LoginService;

public class LoginAction implements Action {

	public String execute(ActionForm form) {
		// TODO Auto-generated method stub
		
		String returnStr ;
		
		LoginForm loginForm = (LoginForm)form;
		//验证用户
		boolean checkResult = LoginService.checkUser(loginForm.getUsername(),loginForm.getPassword());

		//设置转发地址
		if(checkResult){
			returnStr = " Login Success！";
			
		}else{
			returnStr = " Login Error！";
		}
		return loginForm.getUsername()+returnStr;
	}
	
	public boolean execute2(ActionForm form) {
		// TODO Auto-generated method stub
		
		LoginForm loginForm = (LoginForm)form;
		//验证用户
		return  LoginService.checkUser(loginForm.getUsername(),loginForm.getPassword());

	}
}