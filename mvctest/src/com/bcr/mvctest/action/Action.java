package com.bcr.mvctest.action;

import com.bcr.mvctest.form.ActionForm;

public interface Action {

	//定义公用方法，处理form
	public String execute(ActionForm form);
	public boolean execute2(ActionForm form);

}
