package com.bcr.mvctest.utils;

import java.lang.reflect.Field;

import javax.servlet.http.HttpServletRequest;

import com.bcr.mvctest.form.ActionForm;

public class FullBean {
	
	public static ActionForm fullFormByReq(String className,HttpServletRequest request){
		Class clazz;
		ActionForm form = null ;
		try {
			//通过类名得到类
			clazz = Class.forName(className);
			//调用默认构造方法实例化
			form = (ActionForm) clazz.newInstance();
			//得到类的所有属性数组
			Field[] fields = clazz.getDeclaredFields();
			//通过循环给福偶有属性赋值
			for(Field field:fields){
				//设置属性可访问(打破安全协议)
				field.setAccessible(true);
				
				//通过反射设置对象属性的值
				field.set(form, request.getParameter(field.getName()));
				
				//还原属性的
				field.setAccessible(false);
			}
			
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return form;
	}

}
